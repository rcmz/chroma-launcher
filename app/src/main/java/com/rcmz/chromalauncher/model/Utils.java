package com.rcmz.chromalauncher.model;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;

import androidx.annotation.ColorInt;
import androidx.core.graphics.ColorUtils;

public class Utils {
    public static float mapRange(float value, float fromMin, float fromMax, float toMin, float toMax) {
        return (value - fromMin) / (fromMax - fromMin) * (toMax - toMin) + toMin;
    }

    public static float hue(@ColorInt int color) {
        float[] hsv = new float[3];
        Color.colorToHSV(color, hsv);
        return hsv[0];
    }

    public static float saturation(@ColorInt int color) {
        float[] hsv = new float[3];
        Color.colorToHSV(color, hsv);
        return hsv[1];
    }

    public static float value(@ColorInt int color) {
        float[] hsv = new float[3];
        Color.colorToHSV(color, hsv);
        return hsv[2];
    }

    public static double magnitude2D(double x, double y) {
        return Math.sqrt(x*x + y*y);
    }

    public static double distance3D(double[] a, double[] b) {
        double dx = a[0] - b[0];
        double dy = a[1] - b[1];
        double dz = a[2] - b[2];
        return Math.sqrt(dx*dx + dy*dy + dz*dz);
    }

    public static double deltaE(@ColorInt int c1, @ColorInt int c2) {
        double[] lab1 = { 0, 0, 0 }; ColorUtils.colorToLAB(c1, lab1);
        double[] lab2 = { 0, 0, 0 }; ColorUtils.colorToLAB(c2, lab2);
        return distance3D(lab1, lab2);
    }

    @ColorInt
    public static int foregroundColor(@ColorInt int backgroundColor) {
        // bias white foreground
        return deltaE(backgroundColor, Color.BLACK) > Utils.deltaE(backgroundColor, Color.WHITE) * 2 ? Color.BLACK : Color.WHITE;
    }

    public static Bitmap generateBitmap(Drawable drawable, int width, int height, int sharpening) {
        Bitmap bitmap = Bitmap.createBitmap(width * sharpening, height * sharpening, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, width * sharpening, height * sharpening);
        drawable.draw(canvas);
        return Bitmap.createScaledBitmap(bitmap, width, height, false);
    }

    public static String shortName(String fullName) {
        return fullName.substring(fullName.lastIndexOf(".") + 1);
    }
}
