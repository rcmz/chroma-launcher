package com.rcmz.chromalauncher.model;

import android.appwidget.AppWidgetProviderInfo;

import androidx.room.Entity;

@Entity(tableName = "widgetEntries", primaryKeys = { "id" })
public class WidgetEntry {
    public int id;

    public WidgetEntry(int id) {
        this.id = id;
    }
}
