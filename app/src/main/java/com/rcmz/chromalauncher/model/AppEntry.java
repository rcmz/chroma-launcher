package com.rcmz.chromalauncher.model;

import android.content.ComponentName;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;

import com.rcmz.chromalauncher.drawables.CustomIconDrawable;
import com.rcmz.chromalauncher.singletons.Chroma;

@Entity(tableName = "AppEntries", primaryKeys = { "packageName", "className" })
public class AppEntry {
    @NonNull
    public String packageName;

    @NonNull
    public String className;

    @NonNull
    public String label;

    public int iconRes;

    @Ignore
    private Drawable icon;

    @ColorInt
    public int color;

    public boolean hidden;

    public Drawable getIcon() {
        return new CustomIconDrawable(icon, color, false);
    }

    public Drawable getScaledIcon() {
        return new CustomIconDrawable(icon, color, true);
    }

    public AppEntry(ActivityInfo activityInfo, boolean hidden) {
        PackageManager pm = Chroma.getContext().getPackageManager();

        this.hidden = hidden;
        packageName = activityInfo.packageName;
        className = activityInfo.name;
        label = activityInfo.loadLabel(pm).toString();
        icon = activityInfo.loadIcon(pm);
        color = new BetterPalette(icon).getMainColor();

//        Bitmap bitmap = Bitmap.createBitmap(108, 108, Bitmap.Config.ARGB_8888);
//        Palette palette = Palette.from(bitmap).generate();
//        color = palette.getVibrantColor(Color.BLACK);
    }

    public AppEntry(@NonNull String packageName, @NonNull String className, @NonNull String label, @ColorInt int color, boolean hidden) {
        PackageManager pm = Chroma.getContext().getPackageManager();

        this.hidden = hidden;
        this.packageName = packageName;
        this.className = className;
        this.label = label;
        this.color = color;

        try {
            icon = pm.getActivityIcon(new ComponentName(packageName, className));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }
}
