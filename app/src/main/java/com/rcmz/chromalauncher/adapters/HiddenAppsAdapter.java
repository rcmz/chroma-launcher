package com.rcmz.chromalauncher.adapters;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.rcmz.chromalauncher.drawables.CopyDrawable;
import com.rcmz.chromalauncher.model.Utils;
import com.rcmz.chromalauncher.model.AppEntry;
import com.rcmz.chromalauncher.R;

import java.util.List;

public class HiddenAppsAdapter extends BaseAdapter {
    private List<AppEntry> appEntries;

    public HiddenAppsAdapter(List<AppEntry> appEntries) {
        this.appEntries = appEntries;
    }

    @Override
    public int getCount() {
        return appEntries.size();
    }

    @Override
    public AppEntry getItem(int position) {
        return appEntries.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        AppEntry appEntry = getItem(position);
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        TextView textView = (TextView) inflater.inflate(R.layout.hidden_apps_list_item, parent, false);

        textView.setText(appEntry.label);

        Drawable[] defaultDrawables = textView.getCompoundDrawablesRelative();
        Drawable appEntryIcon = appEntry.getIcon();
        appEntryIcon.setBounds(defaultDrawables[0].getBounds());
        textView.setCompoundDrawablesRelative(appEntryIcon, defaultDrawables[1], defaultDrawables[2], defaultDrawables[3]);

        textView.setBackgroundColor(appEntry.color);
        textView.setTextColor(Utils.foregroundColor(appEntry.color));

        return textView;
    }
}
