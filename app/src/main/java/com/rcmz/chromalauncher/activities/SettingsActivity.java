package com.rcmz.chromalauncher.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.rcmz.chromalauncher.R;
import com.rcmz.chromalauncher.fragments.SettingsFragment;

public class SettingsActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        getSupportFragmentManager().beginTransaction().replace(R.id.settings_container, new SettingsFragment()).commit();
    }
}
