package com.rcmz.chromalauncher.activities;

import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import androidx.annotation.ColorInt;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.Observer;
import androidx.preference.PreferenceManager;

import com.rcmz.chromalauncher.model.AppEntry;
import com.rcmz.chromalauncher.model.Utils;
import com.rcmz.chromalauncher.singletons.Chroma;
import com.rcmz.chromalauncher.R;
import com.rcmz.chromalauncher.adapters.AppGridAdapter;
import com.rcmz.chromalauncher.fragments.AppEntryBottomSheetDialogFragment;

import java.util.ArrayList;
import java.util.List;

public class LauncherActivity extends AppCompatActivity {
    private DrawerLayout drawerLayout;
    private GridView gridView;
    private SharedPreferences prefs;
    private Resources res;
    private int backgroundAlpha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.launcher_activity);
        drawerLayout = findViewById(R.id.launcher_activity_drawer_layout);
        gridView = findViewById(R.id.launcher_activity_grid_view);
        prefs = PreferenceManager.getDefaultSharedPreferences(Chroma.getContext());
        res = getResources();

        drawerLayout.addDrawerListener(new DrawerLayout.SimpleDrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slide) {
                super.onDrawerSlide(drawerView, slide);

//                float scrimAlphaF = (float) 0x99 / 0xff;
//                float backgroundAlphaF = (float) backgroundAlpha / 0xff;
//                scrimAlphaF = scrimAlphaF + backgroundAlphaF * (1f - scrimAlphaF);
//                int scrimAlpha = (int) (scrimAlphaF * 0xff);
//                int alpha = (int) Utils.mapRange(slide, 0, 1, backgroundAlpha, scrimAlpha);

                int alpha = (int) Utils.mapRange(slide, 0, 1, backgroundAlpha, 255);

                @ColorInt int color = Color.argb(alpha, 0, 0, 0);
                getWindow().setStatusBarColor(color);
                getWindow().setNavigationBarColor(color);
            }
        });

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AppEntry appEntry = (AppEntry) gridView.getAdapter().getItem(position);
                Intent appIntent = Intent.makeMainActivity(new ComponentName(appEntry.packageName, appEntry.className));
                appIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appIntent, Chroma.slideUpBundle);
            }
        });

        gridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                AppEntry appEntry = (AppEntry) gridView.getAdapter().getItem(position);
                AppEntryBottomSheetDialogFragment fragment = new AppEntryBottomSheetDialogFragment(appEntry);
                fragment.show(getSupportFragmentManager(), fragment.getTag());
                return true;
            }
        });

        Chroma.getRepository().getApps().observe(this, new Observer<List<AppEntry>>() {
            @Override
            public void onChanged(final List<AppEntry> appEntries) {
                updateGrid(appEntries);
                updateBackgroundColor();
            }
        });
    }

    public void updateGrid(final List<AppEntry> appEntries) {
        gridView.post(new Runnable() {
            @Override
            public void run() {
                List<AppEntry> visibleAppEntries = new ArrayList<>();
                List<AppEntry> hiddenAppEntries = new ArrayList<>();

                for (AppEntry a : appEntries) {
                    if (a.hidden) {
                        hiddenAppEntries.add(a);
                    } else {
                        visibleAppEntries.add(a);
                    }
                }

                // set the number of columns
                int columns;

                if (prefs.getBoolean("auto_columns", res.getBoolean(R.bool.pref_def_auto_columns))) {
                    int bestColumns = 1;
                    float bestRatio = 0;

                    for (int c = 1; c <= visibleAppEntries.size(); c++) {
                        int rows = visibleAppEntries.size() / c + (visibleAppEntries.size() % c > 0 ? 1 : 0);
                        int cellWidth = gridView.getWidth() / c;
                        int cellHeight = gridView.getHeight() / rows;
                        float ratio = (float) cellWidth / (float) cellHeight;

                        if (Math.abs(ratio - 1) < Math.abs(bestRatio - 1)) {
                            bestRatio = ratio;
                            bestColumns = c;
                        }
                    }

                    columns = bestColumns;
                } else {
                    columns = prefs.getInt("number_of_columns", res.getInteger(R.integer.pref_def_number_of_columns));
                }

                // complete the grid
                List<AppEntry> appEntriesToShow = new ArrayList<>(visibleAppEntries);

                if (prefs.getBoolean("complete_the_grid", res.getBoolean(R.bool.pref_def_complete_the_grid))) {
                    int rows = visibleAppEntries.size() / columns + (visibleAppEntries.size() % columns > 0 ? 1 : 0);
                    int gridCapacity = columns * rows;
                    Chroma.getRepository().sortApps(hiddenAppEntries);

                    for (int i = 0; i < gridCapacity - visibleAppEntries.size() && i < hiddenAppEntries.size(); i++) {
                        appEntriesToShow.add(hiddenAppEntries.get(i));
                    }
                }

                Chroma.getRepository().sortApps(appEntriesToShow);
                gridView.setNumColumns(columns);
                gridView.setAdapter(new AppGridAdapter(appEntriesToShow));
            }
        });
    }

    private void updateBackgroundColor() {
        if (prefs.getString("background_mode", res.getString(R.string.pref_def_background_mode)).equals("wallpaper")) {
            int wallpaperDarkening = prefs.getInt("wallpaper_darkening", res.getInteger(R.integer.pref_def_wallpaper_darkening));
            backgroundAlpha = (int) Utils.mapRange(wallpaperDarkening, 0, 100, 1, 255);
        } else {
            backgroundAlpha = 255;
        }

        @ColorInt int backgroundColor = Color.argb(backgroundAlpha, 0, 0, 0);
        gridView.setBackgroundColor(backgroundColor);
        getWindow().setStatusBarColor(backgroundColor);
        getWindow().setNavigationBarColor(backgroundColor);
    }

    @Override
    public void onBackPressed() {
        drawerLayout.closeDrawers();
    }
}
