package com.rcmz.chromalauncher.fragments;

import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import com.rcmz.chromalauncher.R;
import com.rcmz.chromalauncher.adapters.HiddenAppsAdapter;
import com.rcmz.chromalauncher.model.AppEntry;
import com.rcmz.chromalauncher.singletons.Chroma;

import java.util.ArrayList;
import java.util.List;

public class HiddenAppsFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.hidden_apps_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        final ListView listView = view.findViewById(R.id.hidden_apps_list_view);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AppEntry appEntry = (AppEntry) listView.getAdapter().getItem(position);
                Intent appIntent = Intent.makeMainActivity(new ComponentName(appEntry.packageName, appEntry.className));
                appIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appIntent, Chroma.slideUpBundle);
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                AppEntry appEntry = (AppEntry) listView.getAdapter().getItem(position);
                AppEntryBottomSheetDialogFragment fragment = new AppEntryBottomSheetDialogFragment(appEntry);
                fragment.show(getActivity().getSupportFragmentManager(), fragment.getTag());
                return true;
            }
        });

        final List<AppEntry> hiddenAppEntries = new ArrayList<>();
        listView.setAdapter(new HiddenAppsAdapter(hiddenAppEntries));

        Chroma.getRepository().getApps().observe(getViewLifecycleOwner(), new Observer<List<AppEntry>>() {
            @Override
            public void onChanged(List<AppEntry> appEntries) {
                hiddenAppEntries.clear();

                for (AppEntry a : appEntries) {
                    if (a.hidden) {
                        hiddenAppEntries.add(a);
                    }
                }

                Chroma.getRepository().sortApps(hiddenAppEntries);
                ((BaseAdapter) listView.getAdapter()).notifyDataSetChanged();
            }
        });
    }
}
