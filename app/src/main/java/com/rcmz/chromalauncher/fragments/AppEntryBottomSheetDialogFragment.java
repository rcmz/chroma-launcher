package com.rcmz.chromalauncher.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.rcmz.chromalauncher.singletons.Chroma;
import com.rcmz.chromalauncher.model.AppEntry;
import com.rcmz.chromalauncher.R;
import com.rcmz.chromalauncher.model.Utils;
import com.rcmz.chromalauncher.activities.SettingsActivity;

public class AppEntryBottomSheetDialogFragment extends BottomSheetDialogFragment {
    private View sheet;
    private AppEntry appEntry;
    private @ColorInt int foregroundColor;
    private @ColorInt int savedNavigationBarColor;

    public AppEntryBottomSheetDialogFragment() {
    }

    public AppEntryBottomSheetDialogFragment(AppEntry appEntry) {
        this.appEntry = appEntry;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable final Bundle savedInstanceState) {
        sheet = inflater.inflate(R.layout.app_entry_bottom_sheet_dialog, container, false);
        sheet.setBackgroundColor(appEntry.color);
        foregroundColor = Utils.foregroundColor(appEntry.color);

        TextView appLabelTextView = sheet.findViewById(R.id.bottom_sheet_dialog_app_label);
        appLabelTextView.setTextColor(foregroundColor);
        appLabelTextView.setText(appEntry.label);
        Drawable appEntryIcon = appEntry.getIcon();
        Drawable defaultDrawable = appLabelTextView.getCompoundDrawablesRelative()[0];
        appEntryIcon.setBounds(defaultDrawable.getBounds());
        appLabelTextView.setCompoundDrawablesRelative(appEntryIcon, null, null, null);

        TextView infoView = sheet.findViewById(R.id.bottom_sheet_dialog_information);
        entry(infoView , new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                intent.setData(Uri.parse("package:" + appEntry.packageName));
                startActivity(intent, Chroma.slideUpBundle);
                dismiss();
            }
        });

        TextView uninstallView = sheet.findViewById(R.id.bottom_sheet_dialog_uninstall);
        entry(uninstallView , new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DELETE);
                intent.setData(Uri.parse("package:" + appEntry.packageName));
                startActivity(intent);
                dismiss();
            }
        });

        TextView showView = sheet.findViewById(R.id.bottom_sheet_dialog_show);
        entry(showView, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appEntry.hidden = false;
                Chroma.getRepository().insertApp(appEntry);
                dismiss();
            }
        });

        TextView hideView = sheet.findViewById(R.id.bottom_sheet_dialog_hide);
        entry(hideView, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appEntry.hidden = true;
                Chroma.getRepository().insertApp(appEntry);
                dismiss();
            }
        });

        TextView settingsView = sheet.findViewById(R.id.bottom_sheet_dialog_launcher_settings);
        entry(settingsView, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Chroma.getContext(), SettingsActivity.class);
                startActivity(intent, Chroma.slideUpBundle);
                dismiss();
            }
        });

        // only show hide or show entry
        if (appEntry.hidden) {
            hideView.setVisibility(View.GONE);
        } else {
            showView.setVisibility(View.GONE);
        }

        // don't show the settings entry if already inside the settings activity
        if (getActivity().getClass().equals(SettingsActivity.class)) {
            settingsView.setVisibility(View.GONE);
        }

        // disable collapsed state
        getDialog().setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                View sheet = ((BottomSheetDialog) dialog).findViewById(com.google.android.material.R.id.design_bottom_sheet);
                BottomSheetBehavior<View> behavior = BottomSheetBehavior.from(sheet);
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                behavior.setSkipCollapsed(true);
            }
        });

        // set navigation bar color
        new Handler(Looper.myLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                Window window = getActivity().getWindow();
                savedNavigationBarColor = window.getNavigationBarColor();
                window.setNavigationBarColor(appEntry.color);
            }
        }, 200);

        return sheet;
    }

    private void entry(TextView textView, View.OnClickListener onClickListener) {
        textView.setTextColor(foregroundColor);
        textView.getCompoundDrawablesRelative()[0].setTint(foregroundColor);
        textView.setOnClickListener(onClickListener);
    }

    @Override
    public void onDestroyView() {
        getActivity().getWindow().setNavigationBarColor(savedNavigationBarColor);
        super.onDestroyView();
    }
}
