package com.rcmz.chromalauncher.fragments;

import android.app.AlertDialog;
import android.appwidget.AppWidgetHost;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProviderInfo;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import com.rcmz.chromalauncher.CustomAppWidgetHost.CustomAppWidgetHost;
import com.rcmz.chromalauncher.R;
import com.rcmz.chromalauncher.CustomAppWidgetHost.CustomAppWidgetHostView;
import com.rcmz.chromalauncher.model.WidgetEntry;
import com.rcmz.chromalauncher.singletons.Chroma;

import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;

public class WidgetsFragment extends Fragment {
    private static final int APPWIDGET_HOST_ID = 42;
    private static final int REQUEST_APPWIDGET_PICK = 9;
    private static final int REQUEST_APPWIDGET_CREATE = 5;
    private AppWidgetManager appWidgetManager;
    private CustomAppWidgetHost appWidgetHost;
    private List<WidgetEntry> widgetEntries;
    private ListView listView;
    private Button addWidgetButton;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.widgets_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        appWidgetManager = AppWidgetManager.getInstance(Chroma.getContext());
        appWidgetHost = new CustomAppWidgetHost(Chroma.getContext(), APPWIDGET_HOST_ID);

        widgetEntries = new ArrayList<>();

        listView = view.findViewById(R.id.widgets_list_view);
        listView.setAdapter(new WidgetsAdapter());

        Chroma.getRepository().getWidgets().observe(getViewLifecycleOwner(), new Observer<List<WidgetEntry>>() {
            @Override
            public void onChanged(List<WidgetEntry> updateWidgetEntries) {
                widgetEntries.clear();
                widgetEntries.addAll(updateWidgetEntries);
                ((BaseAdapter) listView.getAdapter()).notifyDataSetChanged();
            }
        });

        addWidgetButton = new Button(Chroma.getContext());
        addWidgetButton.setText(R.string.add_widget);
        addWidgetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int appWidgetId = appWidgetHost.allocateAppWidgetId();
                Intent pickIntent = new Intent(AppWidgetManager.ACTION_APPWIDGET_PICK);
                pickIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
                startActivityForResult(pickIntent, REQUEST_APPWIDGET_PICK);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_APPWIDGET_PICK:
                    appWidgetPick(data);
                    break;
                case REQUEST_APPWIDGET_CREATE:
                    appWidgetCreate(data);
                    break;
            }
        }
    }

    private void appWidgetPick(Intent data) {
        int appWidgetId = data.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, -1);
        AppWidgetProviderInfo appWidgetInfo = appWidgetManager.getAppWidgetInfo(appWidgetId);

        if (appWidgetInfo.configure != null) {
            Intent intent = new Intent(AppWidgetManager.ACTION_APPWIDGET_CONFIGURE);
            intent.setComponent(appWidgetInfo.configure);
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
            startActivityForResult(intent, REQUEST_APPWIDGET_CREATE);
        } else {
            onActivityResult(REQUEST_APPWIDGET_CREATE, RESULT_OK, data);
        }
    }

    private void appWidgetCreate(Intent data) {
        int appWidgetId = data.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, -1);
        Chroma.getRepository().insertWidget(new WidgetEntry(appWidgetId));
    }

   private class WidgetsAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return widgetEntries.size() + 1;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (position == getCount()-1) {
                return addWidgetButton;
            } else {
                final WidgetEntry widgetEntry = widgetEntries.get(position);
                final AppWidgetProviderInfo appWidgetInfo = appWidgetManager.getAppWidgetInfo(widgetEntry.id);
                CustomAppWidgetHostView appWidgetHostView = (CustomAppWidgetHostView) appWidgetHost.createView(Chroma.getContext(), widgetEntry.id, appWidgetInfo);

                appWidgetHostView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        PackageManager pm = Chroma.getContext().getPackageManager();

                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                        alertDialogBuilder.setTitle(getString(R.string.widget_remove_dialog) + " \"" + appWidgetInfo.loadLabel(pm) + "\" ?");

                        alertDialogBuilder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Chroma.getRepository().removeWidget(widgetEntry);
                            }
                        });

                        alertDialogBuilder.setNegativeButton(R.string.no, null);

                        alertDialogBuilder.show();
                        return true;
                    }
                });

                return appWidgetHostView;
            }
        }
    }
}
