package com.rcmz.chromalauncher.fragments;

import android.os.Bundle;

import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.SeekBarPreference;

import com.rcmz.chromalauncher.R;

public class SettingsFragment extends PreferenceFragmentCompat {
    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.preferences, rootKey);

        findPreference("restart").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                System.exit(0);
                return true;
            }
        });

        listDependency("wallpaper_darkening", "background_mode", "wallpaper");

        valueAsSummary("wallpaper_darkening", "%");
        valueAsSummary("icon_size", "%");
        valueAsSummary("number_of_columns", "");
        valueAsSummary("hue_offset", "°");
    }

    private void valueAsSummary(String key, final String suffix) {
        final SeekBarPreference seekBarPref = findPreference(key);
        seekBarPref.setSummary(seekBarPref.getValue() + suffix);
        seekBarPref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference p, Object newValue) {
                seekBarPref.setSummary(((int) newValue) + suffix);
                return true;
            }
        });
    }

    private void listDependency(String prefKey, String listPrefKey, final String value) {
        final Preference preference = findPreference(prefKey);
        final ListPreference listPreference = findPreference(listPrefKey);
        preference.setEnabled(listPreference.getValue().equals(value));
        listPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference p, Object newValue) {
                preference.setEnabled(((String) newValue).equals(value));
                return true;
            }
        });
    }
}
