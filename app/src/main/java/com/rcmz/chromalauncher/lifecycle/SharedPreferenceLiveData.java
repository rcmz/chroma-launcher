package com.rcmz.chromalauncher.lifecycle;

import android.content.SharedPreferences;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;

public class SharedPreferenceLiveData<T> extends MutableLiveData<T> {
    private SharedPreferences.OnSharedPreferenceChangeListener preferenceListener;
    private T savedValue;

    public SharedPreferenceLiveData(SharedPreferences prefs, LiveData<T> source, final Observer<T> observer) {
        source.observeForever(new Observer<T>() {
            @Override
            public void onChanged(T value) {
                savedValue = value;
                observer.onChanged(value);
            }
        });

        preferenceListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                observer.onChanged(savedValue);
            }
        };

        prefs.registerOnSharedPreferenceChangeListener(preferenceListener);
    }
}

