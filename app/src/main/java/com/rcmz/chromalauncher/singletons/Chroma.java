package com.rcmz.chromalauncher.singletons;

import android.app.ActivityOptions;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;

import com.rcmz.chromalauncher.R;
import com.rcmz.chromalauncher.database.ChromaRepository;

public class Chroma extends Application {
    public static Bundle slideUpBundle;
    public static Chroma instance;
    public static ChromaRepository chromaRepository;

    public void onCreate() {
        super.onCreate();
        instance = this;
        slideUpBundle = ActivityOptions.makeCustomAnimation(this, R.anim.slide_up, R.anim.do_nothing).toBundle();
        chromaRepository = new ChromaRepository();
    }

    public static Context getContext() {
        return instance.getApplicationContext();
    }

    public static ChromaRepository getRepository() {
        return chromaRepository;
    }
}
