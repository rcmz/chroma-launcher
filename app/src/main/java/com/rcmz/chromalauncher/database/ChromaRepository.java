package com.rcmz.chromalauncher.database;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.util.Log;

import androidx.annotation.ColorInt;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.preference.PreferenceManager;
import androidx.room.Room;

import com.rcmz.chromalauncher.R;
import com.rcmz.chromalauncher.activities.LauncherActivity;
import com.rcmz.chromalauncher.lifecycle.SharedPreferenceLiveData;
import com.rcmz.chromalauncher.model.AppEntry;
import com.rcmz.chromalauncher.model.Utils;
import com.rcmz.chromalauncher.model.WidgetEntry;
import com.rcmz.chromalauncher.singletons.Chroma;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ChromaRepository {
    private final ChromaDatabase db;
    private BroadcastReceiver packageChangeBroadcastReceiver;
    private final SharedPreferences prefs;
    private final Resources res;
    private final SharedPreferenceLiveData<List<AppEntry>> apps;
    private final SharedPreferenceLiveData<List<WidgetEntry>> widgets;

    public ChromaRepository() {
        db = Room.databaseBuilder(Chroma.getContext(), ChromaDatabase.class, "chroma-database")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build();
        refreshApps();

        // update the app list if an app in installed / changed / removed
        packageChangeBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Chroma.getRepository().refreshApps();
            }
        };

        IntentFilter packageChangeIntentFilter = new IntentFilter();
        packageChangeIntentFilter.addAction(Intent.ACTION_PACKAGE_ADDED);
        packageChangeIntentFilter.addAction(Intent.ACTION_PACKAGE_CHANGED);
        packageChangeIntentFilter.addAction(Intent.ACTION_PACKAGE_REMOVED);
        packageChangeIntentFilter.addDataScheme("package");
        Chroma.getContext().registerReceiver(packageChangeBroadcastReceiver, packageChangeIntentFilter);

        prefs = PreferenceManager.getDefaultSharedPreferences(Chroma.getContext());
        res = Chroma.getContext().getResources();

        apps = new SharedPreferenceLiveData<>(prefs, db.appsDao().getAll(), new Observer<List<AppEntry>>() {
            @Override
            public void onChanged(List<AppEntry> appEntries) {
                apps.setValue(appEntries);
            }
        });

        widgets = new SharedPreferenceLiveData<>(prefs, db.widgetsDao().getAll(), new Observer<List<WidgetEntry>>() {
            @Override
            public void onChanged(List<WidgetEntry> widgetEntries) {
                widgets.setValue(widgetEntries);
            }
        });
    }

    public LiveData<List<AppEntry>> getApps() {
        return apps;
    }

    public LiveData<List<WidgetEntry>> getWidgets() {
        return widgets;
    }

    public void insertApp(AppEntry appEntry) {
        db.appsDao().insert(appEntry);
    }

    public void insertWidget(WidgetEntry widgetEntry) {
        db.widgetsDao().insert(widgetEntry);
    }

    public void removeWidget(WidgetEntry widgetEntry) {
        db.widgetsDao().delete(widgetEntry);
    }

    private void refreshApps() {
        Log.d("WARNING", "refreshing apps");

        List<AppEntry> newAppEntries = new ArrayList<>();

        // query for all installed apps on the device
        Intent queryIntent = new Intent(Intent.ACTION_MAIN);
        queryIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        List<ResolveInfo> resolveInfos = Chroma.getContext().getPackageManager().queryIntentActivities(queryIntent, 0);

        for (ResolveInfo resolveInfo : resolveInfos) {
            String className = resolveInfo.activityInfo.name;

            // don't create an app entry for the launcher activity
            if (className.equals(LauncherActivity.class.getName())) {
                continue;
            }

            // conserve the hidden state
            AppEntry oldAppEntry = db.appsDao().get(className);
            boolean hidden = oldAppEntry != null && oldAppEntry.hidden;
            AppEntry newAppEntry = new AppEntry(resolveInfo.activityInfo, hidden);
            newAppEntries.add(newAppEntry);
        }

        db.appsDao().deleteAll();
        db.appsDao().insertAll(newAppEntries);
    }

    public void sortApps(List<AppEntry> appEntries) {
        final boolean colorlessAppsFirst = prefs.getBoolean("colorless_apps_first", res.getBoolean(R.bool.pref_def_colorless_apps_first));
        final boolean inverseHueOrder = prefs.getBoolean("inverse_hue_order", res.getBoolean(R.bool.pref_def_inverse_hue_order));
        final float hueOffset = (float) prefs.getInt("hue_offset", res.getInteger(R.integer.pref_def_hue_offset));

        Collections.sort(appEntries, new Comparator<AppEntry>() {
            @Override
            public int compare(AppEntry a1, AppEntry a2) {
                @ColorInt int c1 = a1.color;
                @ColorInt int c2 = a2.color;

                // sort colorless apps
                float s1 = Utils.saturation(c1);
                float s2 = Utils.saturation(c2);
                if (s1 == 0f && s2 == 0f) return a1.packageName.compareTo(a2.packageName);
                if (s1 == 0f) return colorlessAppsFirst ? -1 : +1;
                if (s2 == 0f) return colorlessAppsFirst ? +1 : -1;

                // sort colorful apps
                float h1 = (Utils.hue(c1) + hueOffset) % 360f;
                float h2 = (Utils.hue(c2) + hueOffset) % 360f;
                if (h1 == h2) return a1.packageName.compareTo(a2.packageName);
                return Float.compare(h1, h2) * (inverseHueOrder ? -1 : 1);
            }
        });
    }
}
