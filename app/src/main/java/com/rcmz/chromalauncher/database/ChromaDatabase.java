package com.rcmz.chromalauncher.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.rcmz.chromalauncher.model.AppEntry;
import com.rcmz.chromalauncher.model.WidgetEntry;

@Database(entities = {AppEntry.class, WidgetEntry.class}, version = 5)
public abstract class ChromaDatabase extends RoomDatabase {
    public abstract AppsDao appsDao();
    public abstract WidgetsDao widgetsDao();
}
