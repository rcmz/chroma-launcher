package com.rcmz.chromalauncher.database;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.rcmz.chromalauncher.model.AppEntry;

import java.util.List;

@Dao
public interface AppsDao {
    @Query("SELECT * FROM appentries WHERE className = :className")
    AppEntry get(String className);

    @Query("SELECT * FROM appentries")
    LiveData<List<AppEntry>> getAll();

    @Query("SELECT * FROM  appentries WHERE hidden = 0")
    LiveData<List<AppEntry>> getVisible();

    @Query("SELECT * FROM  appentries WHERE hidden = 1")
    LiveData<List<AppEntry>> getHidden();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<AppEntry> appEntries);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(AppEntry appEntry);

    @Query("DELETE FROM appentries")
    void deleteAll();

    @Query("SELECT COUNT(*) FROM appentries")
    int getCount();
}
