package com.rcmz.chromalauncher.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.rcmz.chromalauncher.model.AppEntry;
import com.rcmz.chromalauncher.model.WidgetEntry;

import java.util.List;

@Dao
public interface WidgetsDao {
    @Query("SELECT * FROM widgetEntries")
    LiveData<List<WidgetEntry>> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(WidgetEntry widgetEntry);

    @Delete
    void delete(WidgetEntry widgetEntry);

}
