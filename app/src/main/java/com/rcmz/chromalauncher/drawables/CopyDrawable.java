package com.rcmz.chromalauncher.drawables;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class CopyDrawable extends Drawable {
    private Drawable source;

    public CopyDrawable(Drawable source) {
        this.source = source;
    }

    @Override
    public void draw(@NonNull Canvas canvas) {
        source.setBounds(getBounds());
        source.draw(canvas);
    }

    @Override
    public void setAlpha(int alpha) {
    }

    @Override
    public void setColorFilter(@Nullable ColorFilter colorFilter) {
    }

    @Override
    public int getOpacity() {
        return PixelFormat.UNKNOWN;
    }
}
