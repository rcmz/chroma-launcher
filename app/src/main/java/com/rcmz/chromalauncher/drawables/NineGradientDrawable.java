package com.rcmz.chromalauncher.drawables;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.view.WindowInsetsAnimation;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.Size;

public class NineGradientDrawable extends Drawable {
    private static final int TL = 0;
    private static final int TM = 1;
    private static final int TR = 2;
    private static final int ML = 3;
    private static final int MM = 4;
    private static final int MR = 5;
    private static final int BL = 6;
    private static final int BM = 7;
    private static final int BR = 8;

    private final int[] colors;
    private final float border;

    public NineGradientDrawable(float border, @Size(9) int[] colors) {
        this.colors = colors;
        this.border = border;
    }

    @Override
    public void draw(@NonNull Canvas canvas) {
        Rect bounds = getBounds();
        Paint paint = new Paint();
        paint.setColor(colors[MM]);

        for (int x = bounds.left; x < bounds.right; x++) {
            for (int y = bounds.top; y < bounds.bottom; y++) {
                canvas.drawPoint(x, y, paint);
            }
        }

//        Rect bounds = copyBounds()
//        Rect centerBounds = copyBounds();
//        centerBounds.inset((int) (border * centerBounds.width()), (int) (border * centerBounds.height()));
//
//        ColorDrawable centerDrawable = new ColorDrawable(colors[C]);
//        centerDrawable.setBounds(centerBounds);
//        centerDrawable.draw(canvas);
//
//        GradientDrawable leftDrawable = new GradientDrawable(GradientDrawable.Orientation.RIGHT_LEFT, new int[] { colors[C], colors[L] });
//        leftDrawable.setBounds(bounds.left, bounds.top, centerBounds.left, bounds.bottom);
//        leftDrawable.draw(canvas);
//
//        GradientDrawable topDrawable = new GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP, new int[] { colors[C], colors[T] });
//        topDrawable.setBounds(bounds.left, bounds.top, bounds.right, centerBounds.top);
//        topDrawable.draw(canvas);
//
//        GradientDrawable rightDrawable = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, new int[] { colors[C], colors[R] });
//        rightDrawable.setBounds(centerBounds.right, bounds.top, bounds.right, bounds.bottom);
//        rightDrawable.draw(canvas);
//
//        GradientDrawable bottomDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[] { colors[C], colors[B] });
//        bottomDrawable.setBounds(bounds.left, centerBounds.bottom, bounds.right, bounds.bottom);
//        bottomDrawable.draw(canvas);
//
//        GradientDrawable gradientDrawable = new GradientDrawable();
//        gradientDrawable.setBounds(getBounds());
//        gradientDrawable.setGradientType(GradientDrawable.SWEEP_GRADIENT);
//
//        int[] sweepColors = new int[5];
//        sweepColors[0] = colors[L];
//        sweepColors[1] = colors[B];
//        sweepColors[2] = colors[R];
//        sweepColors[3] = colors[T];
//        sweepColors[4] = sweepColors[0];
//        gradientDrawable.setColors(sweepColors);
    }

    @Override
    public void setAlpha(int alpha) {
    }

    @Override
    public void setColorFilter(@Nullable ColorFilter colorFilter) {
    }

    @Override
    public int getOpacity() {
        return PixelFormat.UNKNOWN;
    }
}
