package com.rcmz.chromalauncher.drawables;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.AdaptiveIconDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.preference.PreferenceManager;

import com.rcmz.chromalauncher.R;
import com.rcmz.chromalauncher.singletons.Chroma;

public class CustomIconDrawable extends Drawable {
    private Drawable foreground;
    private Drawable background;
    private boolean scaled;
    private boolean isAdaptive;
    private SharedPreferences prefs;
    private Resources res;

    public CustomIconDrawable(Drawable drawable, @ColorInt int color, boolean scaled) {
        prefs = PreferenceManager.getDefaultSharedPreferences(Chroma.getContext());
        res = Chroma.getContext().getResources();
        this.scaled = scaled;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O && drawable instanceof AdaptiveIconDrawable) {
            AdaptiveIconDrawable adaptiveDrawable = (AdaptiveIconDrawable) drawable;
            foreground = adaptiveDrawable.getForeground();
            background = adaptiveDrawable.getBackground();
            isAdaptive = true;
        } else {
            foreground = drawable;
            background = new ColorDrawable(color);
            isAdaptive = false;
        }
    }

    @Override
    public void draw(@NonNull Canvas canvas) {
        int w = getBounds().width();
        int h = getBounds().height();

        Bitmap localBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        Canvas localCanvas = new Canvas(localBitmap);

        float cx = w / 2f;
        float cy = h / 2f;
        float r = Math.min(w, h) / 2f;

        if (scaled) {
            float scale = ((float) prefs.getInt("icon_size", res.getInteger(R.integer.pref_def_icon_size))) / 100f;
            r *= scale;
        }

        if (prefs.getString("icon_shape", res.getString(R.string.pref_def_icon_shape)).equals("default")) {
            Rect bounds = new Rect((int) (cx - r), (int) (cy - r), (int) (cx + r), (int) (cy + r));

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O && isAdaptive) {
                AdaptiveIconDrawable adaptiveIcon = new AdaptiveIconDrawable(background, foreground);
                adaptiveIcon.setBounds(bounds);
                adaptiveIcon.draw(canvas);
            } else {
                foreground.setBounds(bounds);
                foreground.draw(canvas);
            }
        } else {
            Path shapePath = new Path();

            switch (prefs.getString("icon_shape", res.getString(R.string.pref_def_icon_shape))) {
                case "circle":
                    shapePath.addCircle(cx, cy, r, Path.Direction.CCW);
                    break;
                case "square":
                    shapePath.addRect(cx - r, cy - r, cx + r, cy + r, Path.Direction.CCW);
                    break;
                case "rounded_square":
                    shapePath.addRoundRect(cx - r, cy - r, cx + r, cy + r, r / 2f, r / 2f, Path.Direction.CCW);
                    break;
                case "leaf":
                    shapePath.moveTo(cx - r, cy + r);
                    shapePath.lineTo(cx, cy + r);
                    shapePath.arcTo(cx - r, cy - r, cx + r, cy + r, 90, -90, false);
                    shapePath.lineTo(cx + r, cy - r);
                    shapePath.lineTo(cx, cy - r);
                    shapePath.arcTo(cx - r, cy - r, cx + r, cy + r, -90, -90, false);
                    shapePath.close();
                    break;
            }

            if (isAdaptive) {
                r *= 1.5f;
            }

            Rect bounds = new Rect((int) (cx - r), (int) (cy - r), (int) (cx + r), (int) (cy + r));

            background.setBounds(bounds);
            background.draw(localCanvas);

            foreground.setBounds(bounds);
            foreground.draw(localCanvas);

            Path path = new Path();
            path.addRect(0, 0, w, h, Path.Direction.CCW);
            path.op(shapePath, Path.Op.DIFFERENCE);

            Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));

            localCanvas.drawPath(path, paint);
        }

        canvas.drawBitmap(localBitmap, getBounds().left, getBounds().top, new Paint());
    }

    @Override
    public void setAlpha(int i) {
    }

    @Override
    public void setColorFilter(@Nullable ColorFilter colorFilter) {
    }

    @Override
    public int getOpacity() {
        return PixelFormat.UNKNOWN;
    }
}
